# LM Blackboard API

This repository contains backend code for the LM Blackboard App

## Getting Started

```git clone https://vardaro@bitbucket.org/vardaro/lmblackboard-api.git```

```npm install```

```npm run start```

Verify that your API is running properly by visiting this link:

```http://localhost:3000/auth/login```

## Directory Structure

#### Routing
Upon deployment, the server will invoke `npm run start` which initiates the web server. All further api routes should be put in the `/api` to keep everything consistent.

When adding new routes, `export` a function creating these routes and then `require` the corresponding file in `server.js`

#### Sensitive Info

Sensitive info like API keys and such, by convention, should be declared as environment variables on the server. If there is no environment variables present (meaning you are in development) then sensitive info will be store in `/api/secret.js` which `exports` an object containing all sensitive info. This file is <b>NOT</b> for included in the version control because that would defeat the purpose.

## Deployment

The server must be run on a Windows server because it's mode of authentication relies on Security Support Provider Interface (SSPI), a Windows authentication API.