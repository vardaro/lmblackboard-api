const nodeSSPI = require('node-sspi');
const request = require('request');

const EWP_KEY = process.env.EWP_KEY || require('./secret').EWP_KEY;

const auth = app => {
    // inject authentication middleware
    app.use((req, res, next) => {
        new nodeSSPI().authenticate(req, res, err => {
            res.finished || next();
        });
    });

    /**
     * Authenticates who is communicating with the server
     * queries ewp by nt and returns a user
     */
    app.get('/auth/login', (req, res) => {
        let { user } = req.connection;
        // standard format is domain\NT, we only care about their NT
        let NT = user.split('\\')[1];

        let query = `https://api-ewp.global.lmco.com/PersonService?Search=${NT}&APIKey=${EWP_KEY}&ApiOutput=PersonID;Domain;NTID;EmployeeID;DisplayName;JobTitle;Company;City;BusinessAreaName;Email;ManagerDisplayName;ManagerEmail&Output=JSON`;

        request({ url: query, method: 'GET', proxy: null, rejectUnauthorized: false }, (error, response, body) => {
            if (error) {
                console.error(error);
                res.sendStatus(500);
                return;
            }
            res.send(JSON.parse(body)[0]); // the query SHOULD return only one result, which is why we send only the first element in the array
        });
    });




}

module.exports = auth;