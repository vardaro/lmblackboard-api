

const task = app => {

    /**
     * Returns a list of all tasks that can be claimed
     */
    app.get('/task/list', (req, res) => {
        // some db query
    });

    /**
     * Returns a list of tasks claimed by a specified NTID
     */
    app.get('/task/:NTID/claimed', (req, res) => {
        let { NTID } = req.query;

        if (!NTID) {
            res.sendStatus(400);
        }

        // some db query
    });

    /**
     * returns a list of tasks posted by a user
     */
    app.get('/task/:NTID/posted', (req, res) => {
        let { NTID } = req.query;

        if (!NTID) {
            res.sendStatus(400);
        }

        // some db query
    });

    /**
     * returns a list of task completed by a user
     */
    app.get('/task/:NTID/completed', (req, res) => {
        let { NTID } = req.query;

        if (!NTID) {
            res.sendStatus(400);
        }

        // some db query
    });

    /**
     * Creates a new task in the db
     */
    app.post('/task/create', (req, res) => {
        // some db query..........
    });

    /**
     * Get info about a single task by their ID
     */
    app.get('/task/get/:ID', (req, res) => {
        let { ID } = req.query;

        if (!ID) {
            res.sendStatus(400);
        }

        // some db query
    });

    /**
     * Sets an open task to claimed
     */
    app.post('/task/claim_task', (req, res) => {
        //some db put
    });

    /**
     * Sets a claimed task to completed
     */
    app.post('/task/complete_task/:ID', (req, res) => {
        let { ID } = req.query;

        if (!ID) {
            res.sendStatus(400);
        }

        // some db update
    });

}

module.exports = task;