const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors({credentials: true, origin: true}));

// inject routes
require('./api/auth')(app);
require('./api/task')(app);

const PORT = process.env.PORT || 3000; // if local, default port to 3000

app.listen(PORT, () => {
    console.log(`Listening on ${PORT}`);
});
